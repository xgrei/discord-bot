"""Discord Chat bot"""

import os

# import discord
import random
from dotenv import load_dotenv

from discord.ext import commands

load_dotenv()
TOKEN = os.getenv("DISCORD_TOKEN")
GUILD = os.getenv("DISCORD_GUILD")

bot = commands.Bot(command_prefix="!")


@bot.event
async def on_ready():
    print(
        f"{bot.user.name} is connected to the following guild:\n"
    )


@bot.command(
    name="quote", help="Responds with a random quote from #Xagnam irc channel"
)
async def quotes(quote):
    await quote.send(quotes_read())


@bot.command(
    name="lie", help="Responds with a random !lie from #Xagnam irc channel"
)
async def lies(lie):
    await lie.send(lies_read())


def quotes_read():
    """Function to read a random line from text file

    Returns:
        str: one line from LIES.txt
    """
    # Constants
    # Variables

    with open("QUOTES.txt", "r") as quotes:
        str_lines: list[str] = quotes.readlines()
    return random.choice(str_lines).strip()


def lies_read():
    """Function to read a random line from text file

    Returns:
        str: one line from LIES.txt
    """
    # Constants
    # Variables

    with open("LIES.txt", "r") as lies:
        str_lines: list[str] = lies.readlines()
    return random.choice(str_lines).strip()

bot.run(TOKEN)